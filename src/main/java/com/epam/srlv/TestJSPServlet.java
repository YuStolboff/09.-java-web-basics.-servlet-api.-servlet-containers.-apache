package com.epam.srlv;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestJSPServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(TestJSPServlet.class);

    public void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Good> goodList = getJson("src/main/webapp/data/goods.json");
        String userName = "Иванов Иван";
        request.setAttribute("userName", userName);
        String categoryAuto = generatesCatalog(goodList, "auto");
        String categoryMoto = generatesCatalog(goodList, "moto");
        request.setAttribute("categoryAuto", categoryAuto);
        request.setAttribute("categoryMoto", categoryMoto);
        this.getServletContext().getRequestDispatcher("/ServletView.jsp").forward(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private List<Good> getJson(String path) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Catalog catalog = objectMapper.readValue(new FileInputStream(path), Catalog.class);
            return new ArrayList<>(catalog.getGoods());
        } catch (IOException e) {
            log.info("IOException {}", e);
        }
        return null;
    }

    private String generatesCatalog(List<Good> goodList, String category) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Good good : goodList) {
            if (good.getCat().equals(category)) {
                stringBuilder = stringBuilder.append("<br>").append("<div class=\"products\" id=\"")
                        .append(good.getId()).append("\">").append("<div class=\"img\"><img src=\"")
                        .append(good.getImg()).append("\"></img><p>").append(good.getName()).append("<br>")
                        .append("</p></div><div class=\"text_product\">$ ").append(good.getPrice())
                        .append("</div><div><button class=\"button\" id=\"").append(good.getId())
                        .append("\">В корзину</button></div></div>");
            }
        }
        return String.valueOf(stringBuilder);
    }
}
