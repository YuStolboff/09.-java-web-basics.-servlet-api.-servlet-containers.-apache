
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta charset="utf-8">
    <title>Магазин</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/style_modal.css" type="text/css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>

<body>
    <header>
        <div>
            <div id="header_name">Иванов Иван</div>
            <div id="header_exit">
                <a href="autorized.html" target="_self">Выйти</a>
            </div>
            <div id="header_basket">
                <a href="#?w=1600" rel="popup2" class="poplight" id="basket"><button>Корзина</button><button class="count"></button></a>
            </div>
            <div id="header_search">
                <a href="#?w=1600" rel="popup_name" class="poplight"><button>Поиск</button></a>
            </div>
        </div>
    </header>
    <div class="shadow1"></div>
    <div class="field">
    </div>
    <div class="header_category">
        <p class="text_category">Автомобили</p>
    </div>
    <div class="category" id="auto">
        <p class="text_category">Сегодня <%= new java.util.Date()%></p>
        <<h3>Месяцы года:</h3>
                 <% if (request.getAttribute("months") != null) {
                         for (String item : (String[]) request.getAttribute("months")) {
                             out.println("<p>" + item + "</p>");
                         }
                     }
                 %>

    </div>
    <div class="header_category">
        <p class="text_category">Мотоциклы</p>
    </div>
    <div class="category" id="moto">
    </div>

    <footer class="footer">
        <a href="registration.html" target="_self" id="text_footer">Зарегистрироваться</a>
        <p id="text_footer">
            Stolbov production
        </p>

    </footer>

    <div id="popup_name" class="popup_block">
        <h2>Поиск</h2>
        <form>
            <p><input type="search" name="q" placeholder="Название товара.." autofocus class="search1" value=""></p>
            <p><input type="submit" formaction="" value="Найти" class="submit1"></p>
        </form>
        <div class="found_goods">
        </div>
    </div>
    <div id="popup2" class="popup_block">
        <h2>Корзина</h2>
        <div class="selected_goods">
        </div>
        <div class="text_basket">
            <div class="amount">
                <p>Сумма товаров:</p>
            </div>
            <div class="discount">
                <p>Скидка:</p>
            </div>
            <div class="total">
                <p>Итого:</p>
            </div>
        </div>
        <div class="data_basket">
            <div class="amount_val">
            </div>
            <div class="discount_val">
            </div>
            <div class="total_val">
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/displaysCatalog.js"></script>
    <script type="text/javascript" src="js/showsGoods.js"></script>
    <script type="text/javascript" src="js/genBask.js"></script>
    <script type="text/javascript" src="js/modal.js"></script>
    <script type="text/javascript" src="js/search.js"></script>
</body>

</html>